package com.thoughtworks.vapasi;

public class RomanConverter {

    public int convertRomanToArabicNumber(String roman)  throws IllegalArgumentException{

        int arabicNbr=0;
        int i=0;

        if(!roman.contains("I") && !roman.contains("V") && !roman.contains("X") && !roman.contains("L")
                && !roman.contains("C") && !roman.contains("M")){
            throw new IllegalArgumentException();
        }
        while(i<roman.length()){
            int currentValue=getRomanIntValue(roman.charAt(i));
            int nextValue=0;
            //only 1 char
            if(roman.length() == 1){
                arabicNbr=currentValue;
                break;
            }
            //only 2 char
            if(roman.length() == 2){
                nextValue = getRomanIntValue(roman.charAt(i + 1));

                if (currentValue < nextValue) {
                    arabicNbr = nextValue - currentValue;
                    return arabicNbr;

                } else if (currentValue >= nextValue){
                    arabicNbr = nextValue + currentValue;
                    return arabicNbr;

                }
            }
            //3 or more chars
            if((i+1) < roman.length()-1){

                nextValue = getRomanIntValue(roman.charAt(i + 1));

                if (currentValue < nextValue) {
                    arabicNbr = arabicNbr - currentValue;
                    i++;

                }
                if (currentValue >= nextValue){
                    arabicNbr = arabicNbr + currentValue;
                    i++;
                }
            }else{
                arabicNbr = arabicNbr + currentValue;
                i++;
            }
        }


        return arabicNbr;
    }

    int getRomanIntValue(char romanChar){
        int romanIntValue=0;
        switch(romanChar){
            case 'I': romanIntValue= 1;break;
            case 'V': romanIntValue= 5;break;
            case 'X': romanIntValue=10;break;
            case 'L': romanIntValue=50;break;
            case 'C': romanIntValue=100;break;
            case 'M': romanIntValue=1000;break;
        }
        return romanIntValue;

    }
}
